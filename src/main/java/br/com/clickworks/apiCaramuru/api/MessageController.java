package br.com.clickworks.apiCaramuru.api;

import br.com.clickworks.apiCaramuru.model.Message;
import br.com.clickworks.apiCaramuru.service.MessageService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("message/")
@Api(value = "Message API")
public class MessageController {

    @Autowired
    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @ApiOperation(value = "Send message", response = int.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully sent the message."),
            @ApiResponse(code = 406, message = "Could not send the message.")
    })
    @PostMapping
    public int insertMessage(
            @ApiParam(value = "Message") @RequestBody Message message
    ) {
        return messageService.insertMessage(message);
    }
}
