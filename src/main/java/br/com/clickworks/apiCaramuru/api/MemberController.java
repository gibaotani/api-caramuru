package br.com.clickworks.apiCaramuru.api;

import br.com.clickworks.apiCaramuru.model.Member;
import br.com.clickworks.apiCaramuru.service.MemberService;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.concurrent.ExecutionException;

@RequestMapping("member/")
@RestController
public class MemberController{

    private final MemberService memberService;

    @Autowired
    public MemberController(MemberService memberService) {
        this.memberService = memberService;
    }

    //ADD MEMBER TO DB
    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity insertMember(@RequestBody Member member){

        switch (memberService.addMember(member)){
            case "200":
                return ResponseEntity.ok().build();
            default:
                return ResponseEntity.status(400).build();

        }
    }

    //SEARCH ALL MEMBERS DB
    @GetMapping(produces = "application/json")
    public ResponseEntity readAllMember() throws ExecutionException, InterruptedException {

        String respBody = this.memberService.readAllMemberDB();
        switch (respBody){
            case "400" :
                return ResponseEntity.status(400).build();
            default:
                return ResponseEntity.status(200).body(respBody);
        }
    }

    //SEARCH MEMBERS BY ID ON DB
    @GetMapping(params = "id", produces = "application/json")
    public ResponseEntity readMember(@RequestParam String id) throws ExecutionException, InterruptedException {

        String respBody = memberService.readMemberDB(id);
        switch (respBody){
            case "400" :
                return ResponseEntity.status(400).build();
            default:
                return ResponseEntity.status(200).body(respBody);
        }
    }

    //SEARCH MEMBER BY 3 LETTERS ON DB
    @GetMapping(path="search/", produces = "application/json")
    public ResponseEntity searchMember(@RequestParam String param) throws ExecutionException, InterruptedException {

        String respBody = memberService.searchMember(param);
        switch (respBody){
            case "400":
                return ResponseEntity.status(400).build();
            default:
                return ResponseEntity.status(200).body(respBody);
        }

    }

    //UPDATE MEMBER ON DB
    @PutMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity updateMember(@RequestBody Member member)
            throws ExecutionException, InterruptedException {

        String respBody = memberService.updateMemberDB(member);

        switch (respBody){
            case "400" :
                return ResponseEntity.status(400).build();
            default:
                return ResponseEntity.status(200).body(respBody);
        }


    }

    //DELETE MEMBER ON DB
    @DeleteMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity deleteMember(@RequestBody String id) throws ExecutionException, InterruptedException {
        JsonObject json = new JsonParser().parse(id).getAsJsonObject();
        switch (memberService.deleteMemberDB(json.get("id").getAsString())){
            case "200" :
                return ResponseEntity.status(200).build();
            default:
                return ResponseEntity.status(400).build();
        }
    }

}
