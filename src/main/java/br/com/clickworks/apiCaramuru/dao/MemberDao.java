package br.com.clickworks.apiCaramuru.dao;

import br.com.clickworks.apiCaramuru.model.Member;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

public interface MemberDao {

    String insertMember (String id, Member member);

    default String insertMember(Member member){
        String id = UUID.randomUUID().toString();
        return insertMember(id, member);
    }

    // Update a single Member based on CPF for searching, field and alt for alteration
    String updateMemberDB(Member member) throws ExecutionException, InterruptedException;

    // Delete a single Member based on CPF for search
    String deleteMemberDB(String id) throws ExecutionException, InterruptedException;

    // Read a single Member based on CPF for search
    String readMemberDB(String CPF) throws ExecutionException, InterruptedException;

    // Read all Members in a Database
    String readAllMemberDB() throws  ExecutionException, InterruptedException;

    // Search Members by name that contains param;
    String searchMemberDB(String param) throws  ExecutionException, InterruptedException;


}
