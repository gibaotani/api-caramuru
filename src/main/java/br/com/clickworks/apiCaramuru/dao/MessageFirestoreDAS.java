package br.com.clickworks.apiCaramuru.dao;

import br.com.clickworks.apiCaramuru.model.Message;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;
import com.google.gson.Gson;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Repository("CloudFirestore")
public class MessageFirestoreDAS implements MessageDao {

    private final Firestore db;
    private final CollectionReference messages;

    public MessageFirestoreDAS() {
        FirestoreOptions firestoreOptions =
                FirestoreOptions.getDefaultInstance().toBuilder()
                        .setProjectId("api-caramuru")
                        .build();
        db = firestoreOptions.getService();
        messages = db.collection("message");
    }

    private boolean isMessageValid(Message message) {
        if (message.getBody() == "" ||
            message.getFrom() == null ||
            message.getTo() == null ||
            message.getCreationDate().toString() == "")
        {
            return false;
        }
        return true;
    }

    @Override
    public int insertMessage(Message message, ZonedDateTime creationDate) {
        if (isMessageValid(message)) {
            Map<String, Object> docData = new HashMap<>();
            docData.put("from", message.getFrom());
            docData.put("to", message.getTo());
            docData.put("subject", message.getSubject());
            docData.put("body", message.getBody());
            docData.put("creationDate", message.getCreationDate());
            try {
                messages.document().set(docData).get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
            Gson gson = new Gson();
            String json = gson.toJson(docData);
            return 1;
        }
        return 0;
    }
}
