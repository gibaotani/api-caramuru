package br.com.clickworks.apiCaramuru.dao;

import br.com.clickworks.apiCaramuru.model.Message;

import java.time.ZonedDateTime;

public interface MessageDao {

    int insertMessage(Message message,
                      ZonedDateTime creationDate);

    default int insertMessage(Message message) {
        ZonedDateTime creationDate = ZonedDateTime.now();
        return insertMessage(message, creationDate);
    }
}
