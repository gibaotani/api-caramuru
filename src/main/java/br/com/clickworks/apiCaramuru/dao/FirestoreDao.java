package br.com.clickworks.apiCaramuru.dao;

import br.com.clickworks.apiCaramuru.model.Member;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.google.gson.Gson;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;

@Repository("cloudFirestore")
public class FirestoreDao implements MemberDao {

    private  final Firestore db;
    private  final CollectionReference members;

    public FirestoreDao() throws IOException {
        FirestoreOptions firestoreOptions =
                FirestoreOptions.getDefaultInstance().toBuilder()
                        .setProjectId("api-caramuru")
                        .build();
        db = firestoreOptions.getService();
        members = db.collection("membro");
    }


    /*---ON HOLD ---
    public String validateMember(String CPF){

        try {
            Query query = members.whereEqualTo("CPF",CPF);
            ApiFuture<QuerySnapshot> querySnapshot = query.get();

            if(querySnapshot.get().getDocuments().size()>0){
                return "1";
            }else{
                return "0";
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return "Unknown Error";
        }
    }*/
    //Insert Member on Cloud Firestore
    @Override
    public String insertMember(String id, Member member) {
        if (member!=null) {
            try {
                Map<String, Object> docData = new HashMap<>();
                docData.put("_id",id);
                docData.put("name", member.getName());
                docData.put("docIdent", new ObjectMapper()
                        .convertValue(member.getDocIdent(), new TypeReference<Map<String,Object>>(){
                        }));
                docData.put("idUEB", member.getIdUEB());
                docData.put("linkPhoto", member.getLinkPhoto());
                docData.put("birthDate", member.getBirthDate());
                docData.put("active",true);
                members.document(id).set(docData).get();
                String json = new Gson().toJson(docData);
                return "200";
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return e.toString();
            }
        }else {
            return "400";
        }
    }

    //Update a member on Cloud Firestore
    @Override
    public String updateMemberDB(Member member) throws ExecutionException, InterruptedException {
        if (member!=null) {
            try {
                Map<String, Object> docData = new HashMap<>();
                docData.put("_id", member.get_id());
                docData.put("name", member.getName());
                docData.put("idUEB", member.getIdUEB());
                docData.put("docIdent",  new ObjectMapper()
                        .convertValue(member.getDocIdent(), new TypeReference<Map<String,Object>>(){
                        }));
                docData.put("birthDate", member.getBirthDate());
                docData.put("linkPhoto", member.getLinkPhoto());
                docData.put("active", member.isActive());
                members.document(member.get_id()).set(docData).get();
                return new Gson().toJson(docData);
            }catch (Exception e){
                e.printStackTrace();
                return e.toString();
            }
        }else {
            return "400";
        }
    }

    //Delete a member on CLoud Firestore
    @Override
    public String deleteMemberDB(String id) throws ExecutionException, InterruptedException{
        if (id!=null) {
            try{
                Query query = members.whereEqualTo("_id",id);
                ApiFuture<QuerySnapshot> querySnapshot = query.get();
                DocumentSnapshot document = querySnapshot.get().getDocuments().get(0);
                members.document(document.getId()).update("active",false).get();
                return "200";
            }catch (Exception e){
                e.printStackTrace();
                return e.toString();
            }
        } else {
            return "400";
        }
    }

    //Search  Member by id on Cloud Firestore
    @Override
    public String readMemberDB(String id)throws ExecutionException, InterruptedException{

        if (id!=null){
            try {
                Query query = members.whereEqualTo("_id", id);
                ApiFuture<QuerySnapshot> querySnapshot = query.get();
                DocumentSnapshot document = querySnapshot.get().getDocuments().get(0);
                Member member = document.toObject(Member.class);
                String json = new Gson().toJson(member);
                return json;
            }catch (Exception e){
                e.printStackTrace();
                return e.toString();
            }
        } else {
            return "400";
        }
    }

    //Search all Members on Cloud Firestore
    @Override
    public String readAllMemberDB() throws ExecutionException, InterruptedException {
        Query query = members.whereEqualTo("active",true);
        ApiFuture<QuerySnapshot> future = query.get();
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
        if(!documents.isEmpty()) {
            List<Member> membroList = new ArrayList<>();
            for (QueryDocumentSnapshot document : documents) {
                membroList.add(document.toObject(Member.class));
            }
            String data = new Gson().toJson(membroList);
            return data;
        }else{
            return "Nenhum membro registrado";
        }
    }

    //Search Members that contains param Chars on Cloud Firestore
    @Override
    public String searchMemberDB(String param) throws ExecutionException, InterruptedException {
        if(param.isBlank()){
            return readAllMemberDB();
        }else{
            Query query = members.whereEqualTo("active",true);
            ApiFuture<QuerySnapshot> future = query.get();
            List<QueryDocumentSnapshot> documents = future.get().getDocuments();
            List<Member> membroList  = new ArrayList<>();

            for (QueryDocumentSnapshot document : documents) {
                membroList.add(document.toObject(Member.class));
            }

            String result = searchList(membroList,param);
            if(result.isBlank()){
                return "400";
            }else{
                return result;
            }
        }
    }
    public String searchList(List<Member> membroList, String param){

        StringBuilder stb = new StringBuilder();
        stb.append(param);
        char actual =  param.charAt(0);
        List<Member>  membroListTemp = new ArrayList<>();

        for (int i = 0; i < membroList.size(); i++){
            if (membroList.get(i).getName().indexOf(actual)!=-1){
                   membroListTemp.add(membroList.get(i));
            }
        }
        stb.deleteCharAt(0);
        if(!stb.toString().isBlank()) {
            return searchList(membroListTemp, stb.toString());
        }else{
            return new Gson().toJson(membroListTemp);
        }

    }

}
