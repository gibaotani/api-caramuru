package br.com.clickworks.apiCaramuru.dao;

import br.com.clickworks.apiCaramuru.model.Patrol;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

// This is interface is WIP

public interface PatrolDao {

    String insertPatrol(String id, Patrol patrol);

    default String insertPatrol(Patrol patrol){
        String id = UUID.randomUUID().toString();
        return insertPatrol(id, patrol);
    }

    //Update a single Patrol located by name , with designated field and alteration.
    String updatePatrolDB(String name, String field, String alt)throws ExecutionException, InterruptedException;

    // Delete a single Patrol from database
    String deletePatrolDB(String name) throws ExecutionException, InterruptedException;

    //Read a single Patrol based on name from database
    String readPatrol(String name) throws ExecutionException, InterruptedException;

    // Read all Patrols from database
    String readAllPatrol() throws  ExecutionException, InterruptedException;

}
