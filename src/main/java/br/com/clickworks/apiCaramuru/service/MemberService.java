package br.com.clickworks.apiCaramuru.service;

import br.com.clickworks.apiCaramuru.dao.MemberDao;
import br.com.clickworks.apiCaramuru.model.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;


@Service
public class MemberService {

    private final MemberDao memberDao;

    @Autowired
    public MemberService(@Qualifier("cloudFirestore") MemberDao memberDao) {
        this.memberDao = memberDao;
    }

    // Insert a member on the database
    public String addMember(Member member){
        return memberDao.insertMember(member);
    }


    //Read all member on the database and return as JSON
    public String  readAllMemberDB() throws ExecutionException, InterruptedException {
        return memberDao.readAllMemberDB();
    }

    //Read a single member on the database and return as JSON
    public String readMemberDB(String id) throws ExecutionException, InterruptedException {
        return memberDao.readMemberDB(id);
    }

    public  String searchMember(String param) throws ExecutionException, InterruptedException {
        return memberDao.searchMemberDB(param);
    }

    //Delete a single member on the database
    public  String deleteMemberDB(String id) throws ExecutionException, InterruptedException {
        return memberDao.deleteMemberDB(id);
    }

    //Update a single member on the database
    public String updateMemberDB(Member member) throws ExecutionException, InterruptedException {
        return  memberDao.updateMemberDB(member);
    }


}
