package br.com.clickworks.apiCaramuru.service;

import br.com.clickworks.apiCaramuru.dao.MessageDao;
import br.com.clickworks.apiCaramuru.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

    private final MessageDao messageDao;

    @Autowired
    public MessageService(@Qualifier("CloudFirestore") MessageDao messageDao) {
        this.messageDao = messageDao;
    }

    public int insertMessage(Message message) {
        return messageDao.insertMessage(message);
    }

}
