package br.com.clickworks.apiCaramuru.model;

public class DocIdent {

    private String type;
    private String number;

    public DocIdent(){}

    public DocIdent(String type, String number){
            this.setType(type);
            this.setNumber(number);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
