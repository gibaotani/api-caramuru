package br.com.clickworks.apiCaramuru.model;

import java.util.List;

public class Patrol {

    private String _id;
    private String name;
    private List<String> members;

    public Patrol(){}

    public Patrol(String _id, String name, List<String> members){
        this.set_id(_id);
        this.setName(name);
        this.setMembers(members);
    }

    public String get_id() { return _id; }

    public void set_id(String _id) { this._id = _id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public List<String> getMembers() { return members; }

    public void setMembers(List<String> members) { this.members = members; }
}
