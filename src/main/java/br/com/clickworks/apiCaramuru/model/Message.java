package br.com.clickworks.apiCaramuru.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.ZonedDateTime;

public class Message {

    private String id;
    private Member from;
    private Member to;
    private String subject;
    private String body;
    private ZonedDateTime creationDate;

    public Message(@JsonProperty("from") Member from,
                   @JsonProperty("to") Member to,
                   @JsonProperty("subject") String subject,
                   @JsonProperty("body") String body) {
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.body = body;
    }

    public String getId() { // automatically generated id
        return id;
    }

    public Member getFrom() {
        return from;
    }

    public Member getTo() {
        return to;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public ZonedDateTime getCreationDate() {
        return creationDate;
    }
}
