package br.com.clickworks.apiCaramuru.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.cloud.Timestamp;

public class Member {

    private  String _id;
    private  String name;
    private  String idUEB;
    @JsonFormat(pattern = "yyyy-MM-ddTHH:mm:ss.SSS")
    private Timestamp birthDate;
    private  String linkPhoto;
    private DocIdent docIdent;
    private boolean active;

    public Member(){}

    public Member(@JsonProperty("_id")String _id,
                  @JsonProperty("name") String name,
                  @JsonProperty("docIdent") DocIdent docIdent,
                  @JsonProperty("idUEB") String idUEB,
                  @JsonProperty("birthDate") String birthDate,
                  @JsonProperty("linkPhoto") String linkPhoto,
                  @JsonProperty("active") boolean active)
                  {

        this.set_id(_id);
        this.setName(name);
        this.setDocIdent(docIdent);
        this.setIdUEB(idUEB);
        this.setBirthDate(Timestamp.parseTimestamp(birthDate));
        this.setLinkPhoto(linkPhoto);
        this.setActive(active);
    }

    public String get_id() { return _id; }

    public void set_id(String _id) { this._id = _id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getIdUEB() { return idUEB; }

    public void setIdUEB(String idUEB) { this.idUEB = idUEB; }

    public Timestamp getBirthDate() { return birthDate; }

    public void setBirthDate(Timestamp birthDate) { this.birthDate = birthDate; }

    public String getLinkPhoto() { return linkPhoto; }

    public void setLinkPhoto(String linkPhoto) { this.linkPhoto = linkPhoto; }

    public DocIdent getDocIdent() { return docIdent; }

    public void setDocIdent(DocIdent docIdent) { this.docIdent = docIdent; }

    public boolean isActive() { return active; }

    public void setActive(boolean active) { this.active = active; }

}

